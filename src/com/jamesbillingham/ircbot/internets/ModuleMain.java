package com.jamesbillingham.ircbot.internets;

import com.jamesbillingham.ircbot.IrcBotEvents;
import com.jamesbillingham.ircbot.IrcBot;
import com.jamesbillingham.ircbot.Error;
import com.jamesbillingham.ircbot.Module;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ModuleMain implements Module
{
    private IrcBot handler;
    private Properties internets = new Properties();
    private IrcBotEvents listener;

    public ModuleMain() { }

    @Override
    public void Initialize(IrcBot pHandler)
    {
        handler = pHandler;
        
        listener = new IrcBotEvents()
        {
            public void onMessage(String channel, String sender, String message)
            {
               MessageReceived(channel, sender, message);
            }
        };
        
        handler.AddEventListener(listener);
        
        try
        {
            FileReader reader = new FileReader("internets.txt");
            internets.load(reader);
            reader.close();
        }
        catch (FileNotFoundException ex) { Logger.getLogger(ModuleMain.class.getName()).log(Level.SEVERE, null, ex); }
        catch (IOException ex) { Logger.getLogger(ModuleMain.class.getName()).log(Level.SEVERE, null, ex); }
    }
    
    @Override
    public void Destroy()
    {
        handler.RemoveEventListener(listener);
        handler = null;
    }
    
    private void MessageReceived(String channel, String sender, String message)
    {
        if (message.startsWith("!"))
        {
            String[] parts = message.split(" ");
            if (parts[0].equalsIgnoreCase("!internets"))
            {
                if (parts.length == 1) { handler.sendMessage(channel, sender + ": You have " + CountNickInternets(sender) + " internets."); }
                else
                {
                    if (parts[1].equalsIgnoreCase("help"))
                    {
                        handler.sendMessage(channel, "Use as follows:-");
                        handler.sendMessage(channel, "- !internets - Shows you how many internets you have.");
                        handler.sendMessage(channel, "- !internets help - Shows this message.");
                        handler.sendMessage(channel, "- !internets give [nickname] - Gives [nickname] one internet.");
                        handler.sendMessage(channel, "- !internets count [nickname] - Shows how many internets [nickname] has.");
                    }

                    else if (parts[1].equalsIgnoreCase("give"))
                    {
                        if (parts.length < 3) { handler.sendError(channel, Error.User, "Nickname needs to be specified."); }
                        else
                        {
                            String nickname = handler.ContainsNickname(channel, parts[2]);
                            if (nickname == null) { handler.sendError(channel, Error.User, "Nickname needs to be in channel."); }
                            else if (nickname.equalsIgnoreCase(sender)) { handler.sendError(channel, Error.User, "Greedy fuck."); }
                            else
                            {
                                GiveNickInternet(nickname);
                                handler.sendMessage(channel, nickname + " has been given an internet. Congratulations!");
                            }
                        }
                    }

                    else if (parts[1].equalsIgnoreCase("count"))
                    {
                        if (parts.length < 3) { handler.sendError(channel, Error.User, "Nickname needs to be specified."); }
                        else
                        {
                            String nickname = handler.ContainsNickname(channel, parts[2]);
                            if (nickname == null) { handler.sendError(channel, Error.User, "Nickname needs to be in channel."); }
                            else { handler.sendMessage(channel, nickname + " has " + CountNickInternets(nickname) + " internets."); }
                        }
                    }
                    
                    else { handler.sendError(channel, Error.User, "This action doesn't exist. Use \\\"!internets help\\\" for a list of actions."); }
                }
            }
        }
    }

    private int CountNickInternets(String nickname)
    {
        try { return Integer.parseInt((String)internets.get(nickname)); }
        catch (Exception ex) { return 0; }
    }

    private void GiveNickInternet(String nickname)
    {
        internets.setProperty(nickname, new Integer(CountNickInternets(nickname) + 1).toString());

        try
        {
            Writer writer = new FileWriter("internets.txt");
            internets.store(writer, "Internets module.");
            writer.close();
        }
        catch (IOException ex) { Logger.getLogger(ModuleMain.class.getName()).log(Level.SEVERE, null, ex); }
    }
}
