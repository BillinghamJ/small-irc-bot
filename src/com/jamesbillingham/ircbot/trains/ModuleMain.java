package com.jamesbillingham.ircbot.trains;

import com.jamesbillingham.ircbot.IrcBotEvents;
import com.jamesbillingham.ircbot.IrcBot;
import com.jamesbillingham.ircbot.Module;

public class ModuleMain implements Module
{
    private IrcBot handler;
    private IrcBotEvents listener;

    public ModuleMain() { }

    @Override
    public void Initialize(IrcBot pHandler)
    {
        handler = pHandler;
        
        listener = new IrcBotEvents()
        {
            public void onMessage(String channel, String sender, String message)
            {
               MessageReceived(channel, sender, message);
            }
        };
        
        handler.AddEventListener(listener);
    }
    
    @Override
    public void Destroy()
    {
        handler.RemoveEventListener(listener);
        handler = null;
    }
    
    private void MessageReceived(String channel, String sender, String message)
    {
        if (message.contains("trains"))
        {
            handler.sendMessage(channel, "I like trains!");
        }
    }
}
