package com.jamesbillingham.ircbot;

public interface Module {
    public void Initialize(IrcBot pHandler);
    public void Destroy();
}
