package com.jamesbillingham.ircbot;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.EventListenerList;
import org.jibble.pircbot.IrcException;
import org.jibble.pircbot.NickAlreadyInUseException;
import org.jibble.pircbot.PircBot;
import org.jibble.pircbot.User;

public class IrcBot extends PircBot
{
    private Module[] modules = new Module[50];
    private ClassLoader classLoader = IrcBot.class.getClassLoader();
    protected EventListenerList listenerList = new EventListenerList();

    public IrcBot(String server, String nickname, String[] channels, String password) throws Exception
    {
        setName(nickname);
        setLogin(nickname);
        setMessageDelay(0);
        connect(server);
        if (password != null && password.length() > 0) { sendMessage("NickServ", "IDENTIFY " + password); }
        for (String channel : channels)
        {
            joinChannel(channel);
            sendMessage(channel, "Jaymes' bot. PM for information, etc.");
        }
    }

    @Override
    public void onMessage(String channel, String sender, String login, String nickname, String message)
    {
        FireOnMessage(channel, sender, message);
    }

    @Override
    public void onPrivateMessage(String sender, String login, String hostname, String message)
    {
        String[] parts = message.split(" ");
        if (parts[0].equalsIgnoreCase("help"))
        {
            sendMessage(sender, "Use as follows:-");
            sendMessage(sender, "- help - Shows this message.");
            sendMessage(sender, "- join [channel] - Makes the bot join [channel].");
            sendMessage(sender, "- list - Lists loaded module instances including their ids.");
            sendMessage(sender, "- load [module] - Loads and runs instance of [module].");
            sendMessage(sender, "- unload [id] - Unloads module instance [id].");
        }

        else if (parts[0].equalsIgnoreCase("join"))
        {
            if (parts.length < 2) { sendError(sender, Error.User, "Nickname needs to be specified."); }
            else
            {
                if (!parts[1].substring(0, 1).equalsIgnoreCase("#")) { sendError(sender, Error.User, "Not a valid channel."); }
                else {
                    if (ContainsNickname(parts[1], "JaymesBot") != null) { sendError(sender, Error.User, "Already in channel."); }
                    else
                    {
                        joinChannel(parts[1]);
                        sendMessage(parts[1], "Jaymes' bot. PM for information, etc.");
                    }
                }
            }
        }

        else if (parts[0].equalsIgnoreCase("list"))
        {
            sendMessage(sender, "Currently loaded modules:-");
            
            for (int i = 0; i < modules.length; i++)
            {
                if (modules[i] != null)
                {
                    String[] name = modules[i].getClass().getName().split("\\.");
                    sendMessage(sender, "- " + i + " - " + name[name.length - 2]);
                }
            }
        }

        else if (parts[0].equalsIgnoreCase("load"))
        {
            if (parts.length < 2) { sendError(sender, Error.User, "Module needs to be specified."); }
            else
            {
                try {
                    int newId = NewModuleId();
                    if (newId >= 0)
                    {
                        modules[newId] = (Module)classLoader.loadClass("com.jamesbillingham.ircbot." + parts[1].toLowerCase() + ".ModuleMain").newInstance();
                        modules[newId].Initialize(this);
                        sendMessage(sender, "Loaded module " + parts[1].toLowerCase() + ", id " + newId + ".");
                    }
                    else
                    {
                        sendError(sender, Error.User, "Module could not be loaded.");
                    }
                }
                catch (Exception ex) { sendError(sender, Error.User, "Module could not be loaded."); ex.printStackTrace(); }
            }
        }

        else if (parts[0].equalsIgnoreCase("unload"))
        {
            if (parts.length < 2) { sendError(sender, Error.User, "Module instance id needs to be specified."); }
            else
            {
                try {
                    int id = Integer.parseInt(parts[1]);
                    
                    if (modules[id] == null) { sendError(sender, Error.User, "No module with this id loaded."); }
                    else
                    {
                        modules[id].Destroy();
                        modules[id] = null;
                        sendMessage(sender, "Module has been unloaded.");
                    }
                }
                catch (Exception ex) { sendError(sender, Error.User, "Module could not be unloaded."); ex.printStackTrace(); }
            }
        }
        
        else { sendError(sender, Error.User, "This action doesn't exist. Use \\\"help\\\" for a list of actions."); }
    }
    
    @Override
    public void onDisconnect()
    {
        try { reconnect(); }
        catch (IOException ex) { Logger.getLogger(IrcBot.class.getName()).log(Level.SEVERE, null, ex); }
        catch (NickAlreadyInUseException ex) { Logger.getLogger(IrcBot.class.getName()).log(Level.SEVERE, null, ex); }
        catch (IrcException ex) { Logger.getLogger(IrcBot.class.getName()).log(Level.SEVERE, null, ex); }
    }

    public String ContainsNickname(String channel, String nickname)
    {
        User[] users = getUsers(channel);

        for (User user : users)
        {
            String nick = user.getNick();
            if (nick.charAt(0) == '+' || nick.charAt(0) == '%' || nick.charAt(0) == '@' || nick.charAt(0) == '&' || nick.charAt(0) == '~') { nick = nick.substring(1); }
            if (nick.equalsIgnoreCase(nickname)) { return nick; }
        }

        return null;
    }

    public void sendError(String receiver, Error type, String message)
    {
        sendAction(receiver, "throw new System." + ((type == Error.User) ? "WtfIsThisShit" : "JaymesFuckedUp") + "Exception(\"" + message + "\");");
    }
    
    public void AddEventListener(IrcBotEvents listener)
    {
        listenerList.add(IrcBotEvents.class, listener);
    }

    public void RemoveEventListener(IrcBotEvents listener)
    {
        listenerList.remove(IrcBotEvents.class, listener);
    }

    private void FireOnMessage(String channel, String sender, String message)
    {
        Object[] listeners = listenerList.getListenerList();

        for (int i = 0; i < listeners.length; i += 2)
        {
            if (listeners[i] == IrcBotEvents.class) { ((IrcBotEvents)listeners[i + 1]).onMessage(channel, sender, message); }
        }
    }
    
    private int NewModuleId()
    {
        for (int i = 0; i < modules.length; i++)
        {
            if (modules[i] == null) { return i; }
        }
        
        return -1;
    }
}
