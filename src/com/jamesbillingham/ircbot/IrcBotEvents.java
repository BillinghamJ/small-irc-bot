package com.jamesbillingham.ircbot;

import java.util.EventListener;

public interface IrcBotEvents extends EventListener
{
    public void onMessage(String channel, String sender, String message);
}
